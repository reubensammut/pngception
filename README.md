# README #

A C++ implementation of the PNG part of [Ange Albertini's angecryption](https://code.google.com/p/corkami/).

This version performs one 1-block decryption to forge the IV + a full target image decryption which would be embedded in a section with a "loLz" PNG header in the source image.
