//
//  main.cpp
//  CryptIt
//
//  Created by Reuben Sammut on 05/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <openssl/aes.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

void
usage(const char *appname)
{
    std::cerr << "Usage: " << appname << " -k <keyfile> -s <sourcefile> -t <targetfile> -i <ivfile>\n";
    std::cerr << "\t-k <keyfile>    : Key to be used\n";
    std::cerr << "\t-s <sourcefile> : File that contains first PNG\n";
    std::cerr << "\t-t <targetfile> : File that contains second PNG\n";
    std::cerr << "\t-i <ivfile>     : IV file output\n";
    
    exit(EXIT_FAILURE);
}

int main(int argc, char * argv[])
{
    string keyFilePath, sourceFilePath, targetFilePath, ivFilePath;
    
    int ch;
    
    if (argc != 9)
    {
        usage(argv[0]);
    }
    
    while ((ch = getopt(argc, argv, "k:i:s:t:")) != -1)
    {
        switch(ch)
        {
            case 'k': keyFilePath.assign(optarg);
                break;
            case 's': sourceFilePath.assign(optarg);
                break;
            case 't': targetFilePath.assign(optarg);
                break;
            case 'i': ivFilePath.assign(optarg);
                break;
            default:
                usage(argv[0]);
                break;
        }
    }
    
    ifstream is(keyFilePath.c_str(), ios::binary);
    is.seekg(0, std::ios::end);
    streamoff keyLen = is.tellg();
    is.seekg(0, std::ios::beg);
    
    unsigned char *keyData = new unsigned char [keyLen];
    
    is.read((char *)keyData, keyLen);
    
    is.close();
    
    AES_KEY key;
    
    AES_set_encrypt_key(keyData, (int)keyLen * 8, &key);
    
    delete keyData;
    
    is.open(ivFilePath.c_str(), ios::binary);
    unsigned char iv[16];
    is.read((char*)iv,16);
    is.close();
    
    is.open(sourceFilePath.c_str(), ios::binary);
    is.seekg(0, std::ios::end);
    streamoff imgLen = is.tellg();
    is.seekg(0, std::ios::beg);
    
    streamoff newLen = imgLen + (16 - (imgLen %16));
    
    unsigned char *imgData = new unsigned char [newLen];
    
    is.read((char *)imgData, imgLen);
    
    is.close();
    
    unsigned char *imgOut = new unsigned char [newLen];
    
    AES_cbc_encrypt(imgData, imgOut, newLen, &key, iv, AES_ENCRYPT);
    
    ofstream os(targetFilePath.c_str(),ios::binary);
    os.write((char *)imgOut,newLen);
    os.close();
    
    delete imgOut;
    delete imgData;    
    
    return 0;
}
