//
//  PNGHandler.h
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#ifndef __PNGCeption__PNGHandler__
#define __PNGCeption__PNGHandler__

#include <fstream>

#define PNGSIGSIZE 8

class PNGHandler
{
public:
    static void pngception();
private:
    static std::streamoff readImg(const char *path, unsigned char **imgData);
    static bool validatePNGHeader(unsigned char *imgData);
    static std::streamoff buildHackedPNG(unsigned char *mainImg, unsigned char *hiddenData, std::streamoff imgLen, std::streamoff hiddenLen, std::streamoff origHidLen, unsigned char **resultImg);
    static void writeImg(const char *path, unsigned char *data, std::streamoff len);
    static void buildFakeHeader(std::streamoff len, unsigned char **res);
    
    static unsigned char pngsig[PNGSIGSIZE];
    
};

#endif /* defined(__PNGCeption__PNGHandler__) */
