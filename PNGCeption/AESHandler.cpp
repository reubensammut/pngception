//
//  AESHandler.cpp
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#include "AESHandler.h"
#include "Settings.h"

#include <iostream>

void
AESHandler::loadKey(AES_KEY *k)
{
    std::string keyFile = Settings::instance()->getKeyFilePath();
    std::ifstream is(keyFile.c_str(), std::ios::binary);
    is.seekg(0, std::ios::end);
    std::streamoff keyLen = is.tellg();
    is.seekg(0, std::ios::beg);
    
    if (!(keyLen == 16 || keyLen == 24 || keyLen == 32))
    {
        is.close();
        
        std::cerr << "Invalid key length\n";
        
        exit(EXIT_FAILURE);
    }
    
    unsigned char *keyData = new unsigned char [keyLen];
    
    is.read((char *)keyData, keyLen);
    
    is.close();
    
    AES_set_decrypt_key(keyData, (int)keyLen * 8, k);
    
    delete keyData;
}

void
AESHandler::xorBlock(const unsigned char *block1, const unsigned char *block2, unsigned char **res)
{
    int i;
    
    for (i = 0; i < AES_BLOCK_SIZE; ++i)
    {
        (*res)[i] = block1[i] ^ block2[i];
    }
}

void
AESHandler::writeIV(const unsigned char *iv)
{
    std::string ivFile = Settings::instance()->getIVFilePath();
    
    std::ofstream os(ivFile.c_str(), std::ios::binary);
    
    os.write((char *)iv, AES_BLOCK_SIZE);
    
    os.close();

}

std::streamoff
AESHandler::decPNG(const unsigned char *dataToDec, const unsigned char *imgHead, std::streamoff dataLen, unsigned char **res)
{
    AES_KEY decKey;
    loadKey(&decKey);
    
    unsigned char preIV[AES_BLOCK_SIZE];
    unsigned char IV[AES_BLOCK_SIZE];
    unsigned char *p = IV;
    
    AES_ecb_encrypt(dataToDec, preIV, &decKey, AES_DECRYPT);
    xorBlock(preIV, imgHead, &p);
    writeIV(IV);
    
    std::streamoff resLen = dataLen + (AES_BLOCK_SIZE - (dataLen % AES_BLOCK_SIZE));
    
    *res = new unsigned char [resLen];
    memcpy(*res,dataToDec,dataLen);
    
    AES_cbc_encrypt(*res, *res, resLen, &decKey, IV, AES_DECRYPT);
    
    return resLen;
}