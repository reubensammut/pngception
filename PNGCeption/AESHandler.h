//
//  AESHandler.h
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#ifndef __PNGCeption__AESHandler__
#define __PNGCeption__AESHandler__

#include <openssl/aes.h>
#include <fstream>

class AESHandler
{
public:
    static std::streamoff decPNG(const unsigned char *, const unsigned char *, std::streamoff, unsigned char **);
private:
    static void loadKey(AES_KEY *k);
    static void xorBlock(const unsigned char *block1, const unsigned char *block2, unsigned char **res);
    static void writeIV(const unsigned char *iv);

};

#endif /* defined(__PNGCeption__AESHandler__) */
