//
//  PNGHandler.cpp
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

//CRC is done on Header + data

#include "PNGHandler.h"
#include "Settings.h"
#include "AESHandler.h"

#include <iostream>

#include <zlib.h>

unsigned char PNGHandler::pngsig[PNGSIGSIZE] = { 0x89, 'P', 'N', 'G', '\r', '\n', 0x1a, '\n' };

std::streamoff
PNGHandler::readImg(const char *path, unsigned char **imgData)
{
    std::ifstream is(path, std::ios::binary);
    
    is.seekg(0, std::ios::end);
    std::streamoff imgLen = is.tellg();
    is.seekg(0, std::ios::beg);
    
    *imgData = new unsigned char [imgLen];
    
    is.read((char *)*imgData, imgLen);
    
    is.close();
    
    return imgLen;
}

bool
PNGHandler::validatePNGHeader(unsigned char *imgData)
{
    return memcmp(pngsig,imgData,PNGSIGSIZE) == 0;
}

std::streamoff
PNGHandler::buildHackedPNG(unsigned char *mainImg, unsigned char *hiddenData, std::streamoff imgLen, std::streamoff hiddenLen, std::streamoff origHidLen, unsigned char **resultImg)
{
    std::streamoff resLen = hiddenLen // full target image len
                            + sizeof(uLong) // size of crc32 for hidden data
                            + (imgLen - PNGSIGSIZE); // rest of source PNG file
    
    *resultImg = new unsigned char [resLen];
    
    buildFakeHeader(origHidLen, resultImg);
    
    uint32_t reslenh = (uint32_t)origHidLen - (PNGSIGSIZE * 2);
    
    unsigned char *fstart = *resultImg + 12;
    
    unsigned char *p = *resultImg + (PNGSIGSIZE * 2);
    
    memcpy(p, hiddenData + (PNGSIGSIZE * 2), reslenh);
    p += reslenh;
    
    uLong crc = crc32(0L, fstart, reslenh + 4);
    *(uint32_t*)p = htonl(crc % 0x100000000);
    p += 4;
    
    memcpy(p, mainImg + PNGSIGSIZE, imgLen - PNGSIGSIZE);
    
    return resLen;
}

void
PNGHandler::writeImg(const char *path, unsigned char *data, std::streamoff len)
{
    std::ofstream os(path, std::ios::binary);
    
    os.write((char *)data, len);
    
    char rem = len % AES_BLOCK_SIZE;
    
    char i;
    
    for (i = 0; i < rem; ++i)
    {
        os.put(rem);
    }
    
    os.close();
}

void
PNGHandler::buildFakeHeader(std::streamoff len, unsigned char **res)
{
    unsigned char *p = *res;
    
    memcpy(p, pngsig, PNGSIGSIZE);      // PNG header
    p += PNGSIGSIZE;
    
    uint32_t reslenh = (uint32_t)len - (PNGSIGSIZE * 2);
    
    *(uint32_t*)p = htonl(reslenh);     // len of hidden data
    p += 4;
    
    memcpy(p, "loLz", 4);               // header of hidden data
    p += 4;
}

void
PNGHandler::pngception()
{
    std::string src = Settings::instance()->getSourceFilePath();
    std::string tar = Settings::instance()->getTargetFilePath();
    std::string out = Settings::instance()->getOutputFilePath();
    
    unsigned char *srcImg, *tarImg, *resImg, *decImg, *tmpImg;
    std::streamoff srcLen, tarLen, resLen, decLen;
    
    srcLen = readImg(src.c_str(), &srcImg);
    tarLen = readImg(tar.c_str(), &tarImg);
    
    if ( !validatePNGHeader(srcImg) )
    {
        std::cerr << "Source image [" << src << "] is not a PNG file\n";
        delete srcImg;
        delete tarImg;
        exit(EXIT_FAILURE);
    }
    else if ( !validatePNGHeader(tarImg) )
    {
        std::cerr << "Target image [" << tar << "] is not a PNG file\n";
        delete srcImg;
        delete tarImg;
        exit(EXIT_FAILURE);
    }
    
    tmpImg = new unsigned char[AES_BLOCK_SIZE];
    
    buildFakeHeader(tarLen, &tmpImg);
    
    decLen = AESHandler::decPNG(tarImg, tmpImg, tarLen, &decImg);
    
    delete tmpImg;
    
    resLen = buildHackedPNG(srcImg, decImg, srcLen, decLen, tarLen, &resImg);
    
    writeImg(out.c_str(), resImg, resLen);
    
    delete srcImg;
    delete tarImg;
    delete resImg;
    delete decImg;
}

