//
//  main.cpp
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#include <iostream>

#include "PNGHandler.h"
#include "Settings.h"

int main(int argc, char * argv[])
{
    Settings::instance()->parseArgs(argc, argv);
    
    PNGHandler::pngception();
    
    Settings::release();
    
    return 0;
}
