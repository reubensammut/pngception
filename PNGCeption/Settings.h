//
//  Settings.h
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#ifndef __PNGCeption__Settings__
#define __PNGCeption__Settings__

#include <string>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <stdint.h>

class Settings
{
public:
    
    static Settings *instance();
    static void release();

    
    void parseArgs(int argc, char * const argv[]);
    
    std::string &getKeyFilePath();
    std::string &getSourceFilePath();
    std::string &getTargetFilePath();
    std::string &getOutputFilePath();
    std::string &getIVFilePath();
    
private:
    
    Settings();
    ~Settings();
    
    static Settings *singleton;
    
    void usage(const char *appname);
    
    std::string keyFilePath;
    std::string sourceFilePath;
    std::string targetFilePath;
    std::string outputFilePath;
    std::string ivFilePath;
    
};

#endif /* defined(__PNGCeption__Settings__) */
