//
//  Settings.cpp
//  PNGCeption
//
//  Created by Reuben Sammut on 04/07/2015.
//  Copyright (c) 2015 Reuben Sammut. All rights reserved.
//

#include <iostream>
#include <unistd.h>

#include "Settings.h"

Settings *Settings::singleton = NULL;

Settings::Settings() {}
Settings::~Settings() {}

Settings *
Settings::instance()
{
    if (!singleton)
    {
        singleton = new Settings;
    }
    
    return singleton;
}

void
Settings::release()
{
    if (singleton)
        delete singleton;
    
    singleton = NULL;
}

void
Settings::parseArgs(int argc, char * const argv[])
{
    int ch;
    
    if (argc != 11)
    {
        usage(argv[0]);
    }
    
    while ((ch = getopt(argc, argv, "k:o:s:t:i:")) != -1)
    {
        switch(ch)
        {
            case 'k': keyFilePath.assign(optarg);
                break;
            case 's': sourceFilePath.assign(optarg);
                break;
            case 't': targetFilePath.assign(optarg);
                break;
            case 'o': outputFilePath.assign(optarg);
                break;
            case 'i': ivFilePath.assign(optarg);
                break;
            default:
                usage(argv[0]);
                break;
        }
    }
}

void
Settings::usage(const char *appname)
{
    std::cerr << "Usage: " << appname << " -k <keyfile> -s <sourcefile> -t <targetfile> -o <outputfile> -i <ivfile>\n";
    std::cerr << "\t-k <keyfile>    : Key to be used\n";
    std::cerr << "\t-s <sourcefile> : File that contains first PNG\n";
    std::cerr << "\t-t <targetfile> : File that contains second PNG\n";
    std::cerr << "\t-o <outputfile> : The output file\n";
    std::cerr << "\t-i <ivfile>     : IV file output\n";
    
    exit(EXIT_FAILURE);
}

std::string &
Settings::getKeyFilePath()
{
    return keyFilePath;
}

std::string &
Settings::getSourceFilePath()
{
    return sourceFilePath;
}

std::string &
Settings::getTargetFilePath()
{
    return targetFilePath;
}

std::string &
Settings::getOutputFilePath()
{
    return outputFilePath;
}

std::string &
Settings::getIVFilePath()
{
    return ivFilePath;
}